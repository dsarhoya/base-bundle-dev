<?php

namespace App\Security;

use dsarhoya\BaseBundle\Entity\CustomAcl;

class Auth extends CustomAcl{
    //se define un permiso
    protected static $COTIZACIONES = 'Entrar al módulo de cotizaciones';
    //y un título para ese permiso
    protected static $COTIZACIONES_TITLE = 'Modulo cotizaciones';
    protected static $COTIZACIONES_VER_DE_SUCURSAL = 'Ver las cotizaciones de toda la sucursal';

    //acá se define una ayuda para otro permiso
    protected static $COTIZACIONES_VER_DE_SUCURSAL_HELP = 'El usuario solo podrá ver las cotizaciones de la sucursal a la que pertenece';
}
