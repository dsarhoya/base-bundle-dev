<?php

namespace App\Security;

use dsarhoya\BaseBundle\Security\Provider\BaseUserProvider;
use dsarhoya\BaseBundle\Security\Provider\BaseUserProviderInterface;

class UserProvider extends BaseUserProvider implements BaseUserProviderInterface
{
    
}
