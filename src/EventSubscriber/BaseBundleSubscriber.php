<?php

namespace App\EventSubscriber;

use dsarhoya\BaseBundle\Event\BaseBundleEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BaseBundleSubscriber implements EventSubscriberInterface
{
    public function onBaseBundleLogin($event)
    {
        // dd($event);
    }

    public static function getSubscribedEvents()
    {
        return [
            BaseBundleEvents::LOGIN => 'onBaseBundleLogin',
        ];
    }
}
