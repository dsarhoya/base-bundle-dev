<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use dsarhoya\BaseBundle\Entity\BaseUser;
use dsarhoya\BaseBundle\Entity\BaseUserInterface;
use dsarhoya\BaseBundle\Entity\UserKey;

/**
 * User.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser implements BaseUserInterface
{
    /**
     * @ORM\OneToMany(targetEntity="dsarhoya\BaseBundle\Entity\UserKey", mappedBy="user")
     */
    private $keys;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="users")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="Profile", inversedBy="users")
     * @ORM\JoinColumn(name="profile_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $profile;

    public function __construct()
    {
        parent::__construct();
        $this->keys = new ArrayCollection();
    }

    /**
     * @return Collection|UserKey[]
     */
    public function getKeys(): Collection
    {
        return $this->keys;
    }

    public function addKey(UserKey $key): self
    {
        if (!$this->keys->contains($key)) {
            $this->keys[] = $key;
            $key->setUser($this);
        }

        return $this;
    }

    public function removeKey(UserKey $key): self
    {
        if ($this->keys->contains($key)) {
            $this->keys->removeElement($key);
            // set the owning side to null (unless already changed)
            if ($key->getUser() === $this) {
                $key->setUser(null);
            }
        }

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }
}
